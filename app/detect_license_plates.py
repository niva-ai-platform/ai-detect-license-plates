from typing import Any
from transformers import YolosFeatureExtractor, YolosForObjectDetection
import torch
from PIL import Image, ImageFilter


feature_extractor = YolosFeatureExtractor.from_pretrained('./model')
model = YolosForObjectDetection.from_pretrained('./model')


def detect_license_plates(file_in: str, file_out: str, confidence_threshold: float = 0.9) -> dict[str, Any]:
    image = Image.open(file_in)
    inputs = feature_extractor(images=image, return_tensors="pt")
    outputs = model(**inputs)

    target_sizes = torch.tensor([image.size[::-1]])
    results = feature_extractor.post_process(outputs, target_sizes=target_sizes)[0]

    license_plates_detected = False
    output_detections = []
    for score, label, box in zip(results["scores"], results["labels"], results["boxes"]):
        if score >= confidence_threshold and model.config.id2label[label.item()] == 'license-plates':
            license_plates_detected = True
            box = box.tolist()

            output_detections.append({
                'confidence': float(score),
                'startX': box[0],
                'startY': box[1],
                'endX': box[2],
                'endY': box[3]
            })

            image = blur_area(image, box)

    upload_file = False
    if license_plates_detected:
        upload_file = True
        # image.show()
        image.save(file_out)

    tags = ['detect-license-plates'] if license_plates_detected else []
    
    if output_detections:
        max_confidence = max(output_detections, key=lambda x: x['confidence'])['confidence']
    else:
        max_confidence = 0

    output_data = {
        'tags': tags,
        'upload_file': upload_file,
        'results': {
            'targetDetected': license_plates_detected,
            'maxConfidence': max_confidence,
            'detections': output_detections
            }
        }

    return output_data


def blur_area(image: Image, box: list[float], blur_radius: float = 10.0):
    box_int = [round(i) for i in box]
    crop_img = image.crop(box)
    blurred_area = crop_img.filter(ImageFilter.GaussianBlur(radius=10))
    image.paste(blurred_area, box_int)
    return image


if __name__ == '__main__':
    file_in = './sample_images/clear_license_plate_1.jpg'
    file_out = './image_saves/test.jpg'
    confidence_threshold = 0.9
    output = detect_license_plates(file_in, file_out, confidence_threshold=0.9)
    print(output)
