from detect_license_plates import detect_license_plates
from ai_python_detect_wrapper_library import AiDetectWrapper

ai_detect_wrapper = AiDetectWrapper()
ai_detect_wrapper.register_service()
ai_detect_wrapper.create_download_dir()
ai_detect_wrapper.create_upload_dir()
ai_detect_wrapper.initialise_app(detect_license_plates)

app = ai_detect_wrapper.get_app()
